import React, { PropTypes } from 'react';
import classnames from 'classnames';

const ResponsivePropTypes = PropTypes.oneOfType([
  PropTypes.bool,
  PropTypes.oneOf(['desktop', 'mobile', 'tablet']),
  PropTypes.arrayOf(PropTypes.oneOf(['desktop', 'mobile', 'tablet'])),
  PropTypes.shape({ desktop: PropTypes.bool, mobile: PropTypes.bool, tablet: PropTypes.bool }),
]);

const propTypes = {
  children: PropTypes.node,
  isNarrow: ResponsivePropTypes,
  isThreeQuarters: ResponsivePropTypes,
  isTwoThirds: ResponsivePropTypes,
  isHalf: ResponsivePropTypes,
  isOneThird: ResponsivePropTypes,
  isOneQuarter: ResponsivePropTypes,
  is1: ResponsivePropTypes,
  is2: ResponsivePropTypes,
  is3: ResponsivePropTypes,
  is4: ResponsivePropTypes,
  is5: ResponsivePropTypes,
  is6: ResponsivePropTypes,
  is7: ResponsivePropTypes,
  is8: ResponsivePropTypes,
  is9: ResponsivePropTypes,
  is10: ResponsivePropTypes,
  is11: ResponsivePropTypes,
  is12: ResponsivePropTypes,
  isOffsetThreeQuarters: ResponsivePropTypes,
  isOffsetTwoThirds: ResponsivePropTypes,
  isOffsetHalf: ResponsivePropTypes,
  isOffsetOneThird: ResponsivePropTypes,
  isOffsetOneQuarter: ResponsivePropTypes,
  isOffset1: ResponsivePropTypes,
  isOffset2: ResponsivePropTypes,
  isOffset3: ResponsivePropTypes,
  isOffset4: ResponsivePropTypes,
  isOffset5: ResponsivePropTypes,
  isOffset6: ResponsivePropTypes,
  isOffset7: ResponsivePropTypes,
  isOffset8: ResponsivePropTypes,
  isOffset9: ResponsivePropTypes,
  isOffset10: ResponsivePropTypes,
  isOffset11: ResponsivePropTypes,
  isOffset12: ResponsivePropTypes,
};

const defaultProps = {
  children: null,
  isNarrow: false,
  isThreeQuarters: false,
  isTwoThirds: false,
  isHalf: false,
  isOneThird: false,
  isOneQuarter: false,
  is1: false,
  is2: false,
  is3: false,
  is4: false,
  is5: false,
  is6: false,
  is7: false,
  is8: false,
  is9: false,
  is10: false,
  is11: false,
  is12: false,
  isOffsetThreeQuarters: false,
  isOffsetTwoThirds: false,
  isOffsetHalf: false,
  isOffsetOneThird: false,
  isOffsetOneQuarter: false,
  isOffset1: false,
  isOffset2: false,
  isOffset3: false,
  isOffset4: false,
  isOffset5: false,
  isOffset6: false,
  isOffset7: false,
  isOffset8: false,
  isOffset9: false,
  isOffset10: false,
  isOffset11: false,
  isOffset12: false,
};

function getClassNamesForType(type, className) {
  const classNames = {};

  if (Array.isArray(type)) {
    type.forEach(mediaType => { classNames[`${className}-${mediaType}`] = true; });
  } else if (typeof type === 'object') {
    classNames[`${className}-desktop`] = type.desktop;
    classNames[`${className}-mobile`] = type.mobile;
    classNames[`${className}-tablet`] = type.tablet;
  } else if (typeof type === 'string') {
    classNames[`${className}-${type}`] = true;
  } else {
    classNames[`${className}`] = type;
  }

  return classNames;
}

function Column({
  children,
  isNarrow,
  isThreeQuarters, isTwoThirds, isHalf, isOneThird, isOneQuarter,
  is1, is2, is3, is4, is5, is6, is7, is8, is9, is10, is11, is12,
  isOffsetThreeQuarters, isOffsetTwoThirds, isOffsetHalf, isOffsetOneThird, isOffsetOneQuarter,
  isOffset1, isOffset2, isOffset3, isOffset4, isOffset5, isOffset6, isOffset7, isOffset8, isOffset9, isOffset10, isOffset11, isOffset12,
}) {
  const className = {
    ...getClassNamesForType(isNarrow, 'is-narrow'),
    ...getClassNamesForType(isThreeQuarters, 'is-three-quarters'),
    ...getClassNamesForType(isTwoThirds, 'is-two-thirds'),
    ...getClassNamesForType(isHalf, 'is-half'),
    ...getClassNamesForType(isOneThird, 'is-one-third'),
    ...getClassNamesForType(isOneQuarter, 'is-one-quarter'),
    ...getClassNamesForType(is1, 'is-1'),
    ...getClassNamesForType(is2, 'is-2'),
    ...getClassNamesForType(is3, 'is-3'),
    ...getClassNamesForType(is4, 'is-4'),
    ...getClassNamesForType(is5, 'is-5'),
    ...getClassNamesForType(is6, 'is-6'),
    ...getClassNamesForType(is7, 'is-7'),
    ...getClassNamesForType(is8, 'is-8'),
    ...getClassNamesForType(is9, 'is-9'),
    ...getClassNamesForType(is10, 'is-10'),
    ...getClassNamesForType(is11, 'is-11'),
    ...getClassNamesForType(is12, 'is-12'),
    ...getClassNamesForType(isOffsetThreeQuarters, 'is-offset-three-quarters'),
    ...getClassNamesForType(isOffsetTwoThirds, 'is-offset-two-thirds'),
    ...getClassNamesForType(isOffsetHalf, 'is-offset-half'),
    ...getClassNamesForType(isOffsetOneThird, 'is-offset-one-third'),
    ...getClassNamesForType(isOffsetOneQuarter, 'is-offset-one-quarter'),
    ...getClassNamesForType(isOffset1, 'is-offset-1'),
    ...getClassNamesForType(isOffset2, 'is-offset-2'),
    ...getClassNamesForType(isOffset3, 'is-offset-3'),
    ...getClassNamesForType(isOffset4, 'is-offset-4'),
    ...getClassNamesForType(isOffset5, 'is-offset-5'),
    ...getClassNamesForType(isOffset6, 'is-offset-6'),
    ...getClassNamesForType(isOffset7, 'is-offset-7'),
    ...getClassNamesForType(isOffset8, 'is-offset-8'),
    ...getClassNamesForType(isOffset9, 'is-offset-9'),
    ...getClassNamesForType(isOffset10, 'is-offset-10'),
    ...getClassNamesForType(isOffset11, 'is-offset-11'),
    ...getClassNamesForType(isOffset12, 'is-offset-12'),
  };

  return (
    <div className={classnames('column', className)}>
      {children}
    </div>
  );
}

Column.defaultProps = defaultProps;
Column.propTypes = propTypes;

export default Column;
