import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Content from './';

describe('<Content />', () => {
  it('should render html', () => {
    const component = shallow(<Content />);
    expect(component.find('.content').length).to.equal(1);
  });

  it('should add `has-text-centered` className', () => {
    const component = shallow(<Content hasTextCentered />);
    expect(component.find('.content.has-text-centered').length).to.equal(1);
  });

  it('should add `is-large` className', () => {
    const component = shallow(<Content size="large" />);
    expect(component.find('.content.is-large').length).to.equal(1);
  });
});
