import React, { PropTypes } from 'react';
import classnames from 'classnames';

import './content.scss';

const propTypes = {
  children: PropTypes.node,
  hasTextCentered: PropTypes.bool,
  size: PropTypes.oneOf(['small', 'medium', 'large']),
};

const defaultProps = {
  children: null,
  hasTextCentered: false,
  size: null,
};

function Content({ children, size, hasTextCentered }) {
  const className = classnames('content', {
    'has-text-centered': hasTextCentered,
    [`is-${size}`]: !!size,
  });

  return (
    <div className={className}>
      {children}
    </div>
  );
}

Content.defaultProps = defaultProps;
Content.propTypes = propTypes;

export default Content;
