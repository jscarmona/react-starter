import { createElement, PropTypes } from 'react';
import classnames from 'classnames';

const propTypes = {
  children: PropTypes.node,
  isPrimary: PropTypes.bool,
  isInfo: PropTypes.bool,
  isSuccess: PropTypes.bool,
  isWarning: PropTypes.bool,
  isDanger: PropTypes.bool,
  onClose: PropTypes.func,
  tag: PropTypes.oneOf(['p', 'div']),
};

const defaultProps = {
  children: null,
  isPrimary: false,
  isInfo: false,
  isSuccess: false,
  isWarning: false,
  isDanger: false,
  onClose: null,
  tag: 'div',
};

function Notifications({ children, isPrimary, isInfo, isSuccess, isWarning, isDanger, onClose, tag }) {
  const className = classnames('notification', {
    'is-primary': isPrimary,
    'is-info': isInfo,
    'is-success': isSuccess,
    'is-warning': isWarning,
    'is-danger': isDanger,
  });

  const button = typeof onClose === 'function' ?
    createElement('button', { className: 'delete', onClick: onClose }) :
    null;

  return createElement(tag, { className }, button, children);
}

Notifications.defaultProps = defaultProps;
Notifications.propTypes = propTypes;

export default Notifications;
