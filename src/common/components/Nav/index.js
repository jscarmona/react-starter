import React from 'react';

import Container from 'common/components/Container';
import './nav.scss';

function Nav() {
  return (
    <nav className="nav has-shadow">
      <Container>
        <div className="nav-left">
          <a className="nav-item">
            <strong>React Starter</strong>
          </a>
          <a className="nav-item is-tab is-hidden-mobile is-active">Home</a>
          <a className="nav-item is-tab is-hidden-mobile">Features</a>
          <a className="nav-item is-tab is-hidden-mobile">Pricing</a>
          <a className="nav-item is-tab is-hidden-mobile">About</a>
        </div>
        <span className="nav-toggle">
          <span />
          <span />
          <span />
        </span>
        <div className="nav-right nav-menu">
          <a className="nav-item is-tab is-hidden-tablet is-active">Home</a>
          <a className="nav-item is-tab is-hidden-tablet">Features</a>
          <a className="nav-item is-tab is-hidden-tablet">Pricing</a>
          <a className="nav-item is-tab is-hidden-tablet">About</a>
          <a className="nav-item is-tab">Log out</a>
        </div>
      </Container>
    </nav>
  );
}

export default Nav;
