import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Nav from './';

describe('<Nav />', () => {
  it('should render html', () => {
    const component = shallow(<Nav />);
    expect(component.find('.nav').length).to.equal(1);
  });
});
