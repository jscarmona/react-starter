import React, { PropTypes } from 'react';
import classnames from 'classnames';

const propTypes = {
  children: PropTypes.node,
  isFluid: PropTypes.bool,
};

const defaultProps = {
  children: null,
  isFluid: false,
};

function Container({ children, isFluid }) {
  const className = classnames('container', { 'is-fluid': isFluid });

  return (
    <div className={className}>{children}</div>
  );
}

Container.defaultProps = defaultProps;
Container.propTypes = propTypes;

export default Container;
