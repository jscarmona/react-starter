import React, { PropTypes } from 'react';
import classnames from 'classnames';

import './columns.scss';

const propTypes = {
  children: PropTypes.node,
  isDesktop: PropTypes.bool,
  isGapless: PropTypes.bool,
  isMultiline: PropTypes.bool,
  isMobile: PropTypes.bool,
  isTablet: PropTypes.bool,
  isVcentered: PropTypes.bool,
};

const defaultProps = {
  children: null,
  isDesktop: false,
  isGapless: false,
  isMultiline: false,
  isMobile: false,
  isTablet: false,
  isVcentered: false,
};

function Columns({ children, isDesktop, isGapless, isMultiline, isMobile, isTablet, isVcentered }) {
  const className = classnames('columns', {
    'is-desktop': isDesktop,
    'is-gapless': isGapless,
    'is-multiline': isMultiline,
    'is-mobile': isMobile,
    'is-tablet': isTablet,
    'is-vcentered': isVcentered,
  });

  return (
    <div className={className}>
      {children}
    </div>
  );
}

Columns.defaultProps = defaultProps;
Columns.propTypes = propTypes;

export default Columns;
