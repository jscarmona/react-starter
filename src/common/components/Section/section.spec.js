import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Section from './';

describe('<Section />', () => {
  it('should render html', () => {
    const component = shallow(<Section>Hello</Section>);
    expect(component.find('.section').length).to.equal(1);
  });

  it('should add `is-large` className', () => {
    const component = shallow(<Section size="large" />);
    expect(component.find('.section.is-large').length).to.equal(1);
  });

  it('should render title if passed in', () => {
    const component = shallow(<Section title="title" />);
    expect(component.find('.title').text()).to.eq('title');
  });

  it('should render subtitle if passed in', () => {
    const component = shallow(<Section subtitle="subtitle" />);
    expect(component.find('.subtitle').text()).to.eq('subtitle');
  });
});
