import React, { PropTypes } from 'react';
import classnames from 'classnames';

import Container from 'common/components/Container';
import './section.scss';

const propTypes = {
  children: PropTypes.node,
  isFluid: PropTypes.bool,
  size: PropTypes.oneOf(['medium', 'large']),
  subtitle: PropTypes.string,
  title: PropTypes.string,
};

const defaultProps = {
  children: null,
  size: null,
  subtitle: null,
  title: null,
  isFluid: false,
};

function Section({ title, subtitle, children, size, isFluid }) {
  const className = classnames('section', { [`is-${size}`]: !!size });

  return (
    <section className={className}>
      <Container isFluid={isFluid}>
        {
          (title || subtitle) && (
            <header className="heading">
              {title && <h1 className="title">{title}</h1>}
              {subtitle && <h2 className="subtitle">{subtitle}</h2>}
              <hr />
            </header>
          )
        }
        {children}
      </Container>
    </section>
  );
}

Section.defaultProps = defaultProps;
Section.propTypes = propTypes;

export default Section;
