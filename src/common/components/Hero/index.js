import React, { PropTypes } from 'react';
import classnames from 'classnames';

import Container from 'common/components/Container';
import './hero.scss';

const propTypes = {
  isBold: PropTypes.bool,
  color: PropTypes.oneOf(['primary', 'info', 'success', 'warning', 'danger', 'light', 'dark']),
  size: PropTypes.oneOf(['medium', 'large', 'fullheight']),
  subtitle: PropTypes.string,
  title: PropTypes.string,
};

const defaultProps = {
  isBold: false,
  color: 'primary',
  size: 'medium',
  subtitle: null,
  title: null,
};

function Hero({ title, subtitle, size, color, isBold }) {
  const className = classnames('hero', `is-${color}`, `is-${size}`, { 'is-bold': isBold });

  return (
    <section className={className}>
      <div className="hero-body">
        <Container>
          {title && <h1 className="title">{title}</h1>}
          {subtitle && <h2 className="subtitle">{subtitle}</h2>}
        </Container>
      </div>
    </section>
  );
}

Hero.defaultProps = defaultProps;
Hero.propTypes = propTypes;

export default Hero;
