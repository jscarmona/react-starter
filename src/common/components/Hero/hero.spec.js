import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Hero from './';

describe('<Hero />', () => {
  it('should render html', () => {
    const component = shallow(<Hero />);
    expect(component.find('.hero').length).to.equal(1);
  });

  it('should add `is-bold` className', () => {
    const component = shallow(<Hero isBold />);
    expect(component.find('.hero.is-bold').length).to.equal(1);
  });

  it('should add `is-large` className', () => {
    const component = shallow(<Hero size="large" />);
    expect(component.find('.hero.is-large').length).to.equal(1);
  });

  it('should add `is-dark` className', () => {
    const component = shallow(<Hero color="dark" />);
    expect(component.find('.hero.is-dark').length).to.equal(1);
  });

  it('should render title if passed in', () => {
    const component = shallow(<Hero title="title" />);
    expect(component.find('.title').text()).to.eq('title');
  });

  it('should render subtitle if passed in', () => {
    const component = shallow(<Hero subtitle="subtitle" />);
    expect(component.find('.subtitle').text()).to.eq('subtitle');
  });
});
