import React from 'react';

import Container from 'common/components/Container';
import Content from 'common/components/Content';
import './footer.scss';

function Footer() {
  return (
    <footer className="footer">
      <Container>
        <Content hasTextCentered>
          <p>
            <strong>React Starter</strong> by <a href="http://github.com/jscarmona">Javier Carmona</a>. The source code is licensed <a href="http://opensource.org/licenses/mit-license.php">MIT</a>.
          </p>
          <p>
            <a className="icon" href="https://github.com/jscarmona">
              <i className="fa fa-github" />
            </a>
          </p>
        </Content>
      </Container>
    </footer>
  );
}

export default Footer;
