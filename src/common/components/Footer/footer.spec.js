import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Footer from './';

describe('<Footer />', () => {
  it('should render html', () => {
    const component = shallow(<Footer />);
    expect(component.find('.footer').length).to.equal(1);
  });
});
