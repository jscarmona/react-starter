import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';
import Title from './';

describe('<Title />', () => {
  it('should render a title', () => {
    const component = shallow(<Title>Hello</Title>);
    expect(component.find('.title').length).to.equal(1);
  });

  it('should add `is-2` className', () => {
    const component = shallow(<Title size={2}>Hello</Title>);
    expect(component.find('.title.is-2').length).to.equal(1);
  });

  it('should be a h2 tag', () => {
    const component = shallow(<Title tag="h2">Hello</Title>);
    expect(component.find('h2.title').length).to.equal(1);
  });

  it('should render a subtitle', () => {
    const component = shallow(<Title type="subtitle">Hello</Title>);
    expect(component.find('.subtitle').length).to.equal(1);
  });
});
