import React, { PropTypes } from 'react';
import classnames from 'classnames';

import './title.scss';

const propTypes = {
  children: PropTypes.node.isRequired,
  size: PropTypes.oneOf([1, 2, 3, 4, 5, 6]),
  tag: PropTypes.oneOf(['p', 'div', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6']),
  type: PropTypes.oneOf(['title', 'subtitle']),
};

const defaultProps = {
  size: null,
  tag: 'p',
  type: 'title',
};

function Title({ children, size, tag, type }) {
  const className = classnames(type, { [`is-${size}`]: !!size });

  return React.createElement(tag, { className }, children);
}

Title.defaultProps = defaultProps;
Title.propTypes = propTypes;

export default Title;
