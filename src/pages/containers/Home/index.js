import React from 'react';
import { connect } from 'react-redux';

import Column from 'common/components/Column';
import Columns from 'common/components/Columns';
import Content from 'common/components/Content';
import Notification from 'common/components/Notification';
import Section from 'common/components/Section';
import Title from 'common/components/Title';

const Home = () => (
  <div>
    <Section>
      {/* eslint no-alert: 0*/}
      <Notification onClose={() => alert('close me')} isDanger>Lorem ipsum dolor sit amet</Notification>
    </Section>
    <Section title="Grid">
      <Columns isMobile isMultiline>
        <Column is4="tablet" is12="mobile">
          <Notification isInfo>Sidebar</Notification>
        </Column>
        <Column is8="tablet" is12="mobile">
          <Columns isMobile isMultiline>
            <Column is4="desktop" is6={['mobile', 'tablet']}>
              <Notification isSuccess>Hello World</Notification>
            </Column>
            <Column is4="desktop" is6={['mobile', 'tablet']}>
              <Notification isSuccess>Hello World</Notification>
            </Column>
            <Column is4="desktop" is6={['mobile', 'tablet']}>
              <Notification isSuccess>Hello World</Notification>
            </Column>
            <Column is4="desktop" is6={['mobile', 'tablet']}>
              <Notification isSuccess>Hello World</Notification>
            </Column>
            <Column is4="desktop" is6={['mobile', 'tablet']}>
              <Notification isSuccess>Hello World</Notification>
            </Column>
          </Columns>
        </Column>
      </Columns>
    </Section>
    <Section title="Hello World Title" subtitle="Hello World Subtitle">
      <Content>
        <Title tag="h2" size={2}>Hello World!</Title>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio deserunt amet, nisi tenetur, ab, aliquam adipisci saepe fugiat, omnis veritatis eius optio neque. Autem reprehenderit minima soluta earum, dolorem beatae.</p>
        <Title tag="h4" size={4} type="subtitle">Hello World Subtitle</Title>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio deserunt amet, nisi tenetur, ab, aliquam adipisci saepe fugiat, omnis veritatis eius optio neque. Autem reprehenderit minima soluta earum, dolorem beatae.</p>
      </Content>
    </Section>
  </div>
);

const mapStateToProps = () => ({});
const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
