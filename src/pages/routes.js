import React from 'react';
import { Route } from 'react-router';

import Home from 'pages/containers/Home';

export default () => (
  <Route>
    <Route path="/" component={Home} />
  </Route>
);
