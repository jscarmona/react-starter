import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';
import chaiJsx from 'chai-jsx';
import { jsdom } from 'jsdom';

chai.use(chaiEnzyme());
chai.use(chaiJsx);

const applyJsdomWorkaround = (window) => {
  Object.defineProperties(window.HTMLElement.prototype, {
    offsetLeft: {
      get: () => parseFloat(window.getComputedStyle(this).marginLeft) || 0,
    },
    offsetTop: {
      get: () => parseFloat(window.getComputedStyle(this).marginTop) || 0,
    },
    offsetHeight: {
      get: () => parseFloat(window.getComputedStyle(this).height) || 0,
    },
    offsetWidth: {
      get: () => parseFloat(window.getComputedStyle(this).width) || 0,
    },
  });
};

global.document = jsdom('<!doctype html><html><body></body></html>');
global.window = document.defaultView;
global.navigator = global.window.navigator;

applyJsdomWorkaround(global.window);
