import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

const propTypes = {
  params: PropTypes.shape({
    statusCode: PropTypes.string,
  }).isRequired,
  location: PropTypes.shape({
    query: PropTypes.shape({
      url: PropTypes.string,
    }),
  }).isRequired,
};

const NotFound = ({ params: { statusCode }, location: { query } }) => (
  <div className="not-found">
    <Helmet title={`${statusCode}`} />
    <div className="not-found__content">
      <h2>Ooops! Something&apos;s Not Right.</h2>
      <p>The page you are looking for may have been moved or deleted. Click here to return to our homepage.</p>
      <p>
        <small>Code: {statusCode}</small><br />
        <small>Url: {query.url}</small><br />
      </p>
    </div>
  </div>
);

NotFound.propTypes = propTypes;

const mapStateToProps = () => ({});
const mapDispatchToProps = () => ({});

export default connect(mapStateToProps, mapDispatchToProps)(NotFound);
