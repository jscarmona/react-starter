import React, { PropTypes } from 'react';
import { asyncConnect } from 'redux-connect';
import { GatewayProvider, GatewayDest } from 'react-gateway';
import Helmet from 'react-helmet';

import Footer from 'common/components/Footer';
import Hero from 'common/components/Hero';
import Nav from 'common/components/Nav';

import './app.scss';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {
  children: null,
};

function App({ children }) {
  return (
    <GatewayProvider>
      <div>
        <Helmet defaultTitle="react starter" titleTemplate="%s - react starter" />
        <GatewayDest name="top" />
        <Nav />
        <Hero title="Hero Title" subtitle="Hero Subtitle" size="medium" color="dark" isBold />
        {children}
        <Footer />
      </div>
    </GatewayProvider>
  );
}

App.defaultProps = defaultProps;
App.propTypes = propTypes;

const asyncProps = [];
const mapStateToProps = () => ({});
const mapDispatchToProps = () => ({});

export default asyncConnect(asyncProps, mapStateToProps, mapDispatchToProps)(App);
export { App, mapStateToProps, mapDispatchToProps };
