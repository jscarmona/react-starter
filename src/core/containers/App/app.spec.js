import React from 'react';
import { expect } from 'chai';
import { shallow } from 'enzyme';

import { App } from './';

describe('<App />', () => {
  it('should render a <Nav />', () => {
    const component = shallow(<App />);
    expect(component.find('Nav').length).to.equal(1);
  });

  it('should render a <Hero />', () => {
    const component = shallow(<App />);
    expect(component.find('Hero').length).to.equal(1);
  });

  it('should render a <Footer />', () => {
    const component = shallow(<App />);
    expect(component.find('Footer').length).to.equal(1);
  });
});
