import React from 'react';
import { Route } from 'react-router';

import App from 'core/containers/App';
import NotFound from 'core/containers/NotFound';
import getPagesRoutes from 'pages/routes';

export default (store) => (
  <Route component={App}>
    <Route path="error/:statusCode" component={NotFound} />
    {getPagesRoutes(store)}
  </Route>
);
