import axios from 'axios';
import defaultsDeep from 'lodash/defaultsDeep';

export default () => (next) => (action) => {
  if (!Array.isArray(action.type)) {
    return next(action);
  }

  const { type: [REQUEST, SUCCESS, ERROR], payload } = action;
  const config = defaultsDeep({}, payload, {
    method: 'get',
    headers: {},
    baseURL: 'http://localhost:5000',
  });

  next({ type: REQUEST, payload: config });

  return new Promise((resolve) => {
    axios(config)
      .then(
        (response) => {
          next({ type: SUCCESS, payload: response });
          resolve(response);
        },
        (error) => {
          next({ type: ERROR, payload: error });
        }
      );
  });
};
