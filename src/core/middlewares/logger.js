import createLogger from 'redux-logger';

export default createLogger({
  collapsed: true,
  logger: console,
  predicate: (getState, { type }) => (
     type !== '@@redux-form/REGISTER_FIELD' &&
     type !== '@@redux-form/UNREGISTER_FIELD' &&
     type !== '@@redux-form/UPDATE_SYNC_ERRORS' &&
     type !== '@@redux-form/BLUR' &&
     type !== '@@redux-form/CHANGE' &&
     type !== '@@redux-form/FOCUS' &&
     type !== '@@redux-form/TOUCH'
  ),
});
