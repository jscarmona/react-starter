import useScroll from 'react-router-scroll/lib/useScroll';

export default () => useScroll((prevRouterProps, { routes, location }) => {
  if (!routes.some((route) => route.infiniteScroll) || !prevRouterProps) {
    return [0, 0];
  }

  if (
    location.query.pg &&
    parseInt(location.query.pg, 10) > 1 &&
    location.pathname === prevRouterProps.location.pathname
  ) {
    return false;
  }

  return true;
});
