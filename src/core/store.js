import { createStore, applyMiddleware, compose } from 'redux';
import { browserHistory } from 'react-router';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';

import api from 'core/middlewares/api';
import logger from 'core/middlewares/logger';

export const configureStore = (initialState, rootReducer) => {
  const middlewares = [routerMiddleware(browserHistory), api, thunk];
  let devToolsExtension = (f) => f;

  if (process.env.NODE_ENV !== 'production') {
    if (typeof global.document !== 'undefined') {
      middlewares.push(logger);
    }

    if (global.devToolsExtension) {
      devToolsExtension = global.devToolsExtension();
    }
  }

  return createStore(rootReducer, initialState, compose(
    applyMiddleware(...middlewares),
    devToolsExtension
  ));
};
