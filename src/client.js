import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { match, Router, applyRouterMiddleware, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { ReduxAsyncConnect } from 'redux-connect';

import { configureStore } from 'core/store';
import rootReducer from 'core/reducer';
import getRoutes from 'core/routes';
import scroll from 'core/middlewares/scroll';

const store = configureStore(global.__INITIAL_STATE__, rootReducer);
const history = syncHistoryWithStore(browserHistory, store);

match({ history, routes: getRoutes(store) }, (error, redirectLocation, renderProps) => {
  render(
    <Provider store={store}>
      <Router
        {...renderProps}
        render={(props) => <ReduxAsyncConnect {...props} render={applyRouterMiddleware(scroll())} />}
      />
    </Provider>,
    document.getElementById('app')
  );
});
