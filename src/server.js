/* eslint no-console: 0 */
import express from 'express';
import path from 'path';
import responseTime from 'response-time';
import favicon from 'serve-favicon';
import logger from 'morgan';
import chalk from 'chalk';
import compression from 'compression';
import qs from 'qs';
import fs from 'fs';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { match } from 'react-router';
import { ReduxAsyncConnect, loadOnServer } from 'redux-connect';
import createMemoryHistory from 'react-router/lib/createMemoryHistory';
import serialize from 'serialize-javascript';
import { minify } from 'html-minifier';
import Helmet from 'react-helmet';

import { configureStore } from './core/store';
import rootReducer from './core/reducer';
import getRoutes from './core/routes';

const getPathFromManifest = (assetPath, manifest = {}) => manifest[assetPath] || assetPath;
const getManifestFile = () => {
  const manifestFilePath = path.join(__dirname, '..', 'build', 'static', 'manifest.json');

  if (!fs.existsSync(manifestFilePath)) {
    return {};
  }

  try {
    return JSON.parse(fs.readFileSync(manifestFilePath, 'utf8'));
  } catch (e) {
    return {};
  }
};

const createPage = (html, head, state, host) => {
  const manifest = getManifestFile();

  return (
    `<!DOCTYPE html>
    <html lang="en" data-version="${process.env.APP_VERSION}">
    <head>
      <meta charset="UTF-8">
      ${head.title}
      <meta name="viewport" content="width=device-width, initial-scale=1">
      ${head.meta}
      <link rel="stylesheet" href="//${host}/${getPathFromManifest('main.css', manifest)}">
      ${head.link}
      ${head.script}
    </head>
    <body>
      <div id="app">${html}</div>
      <script>window.__INITIAL_STATE__ = ${serialize(state, { haxorXSS: '</script>', isJSON: true })};</script>
      <script src="//${host}/${getPathFromManifest('vendor.js', manifest)}"></script>
      <script src="//${host}/${getPathFromManifest('main.js', manifest)}"></script>
    </body>
    </html>`
  );
};

const app = express();

app.set('port', 3000);

if (process.env.NODE_ENV === 'development') {
  app.use(logger('dev'));
  app.use(responseTime());
}

app.use(compression());
app.use(favicon(path.join(__dirname, '..', 'build', 'static', 'favicon.ico')));
app.use(express.static(path.join(__dirname, '..', 'build', 'static')));

app.use((req, res, next) => {
  if (req.path.substr(-1) === '/' && req.path.length > 1) {
    const query = Object.keys(req.query).length ? `?${qs.stringify(req.query)}` : '';
    res.redirect(301, `${req.path.slice(0, -1)}${query}`);
  } else {
    next();
  }
});

app.use((req, res) => {
  // TODO: Add redis cache layer for html rendering
  const store = configureStore({}, rootReducer);
  const history = createMemoryHistory(req.url);

  match({ history, routes: getRoutes(store), location: req.url }, (error, redirectLocation, renderProps) => {
    loadOnServer({ ...renderProps, store }).then(() => {
      if (error) {
        console.log(chalk.red(error.status), chalk.red(error.message));
        res.redirect(`/error/${error.status || 500}?url=${req.path}`);
      } else if (redirectLocation) {
        const url = `${redirectLocation.pathname}${redirectLocation.search}`;
        console.log(chalk.blue(`Redirecting to ${url}`));
        res.redirect(url);
      } else if (renderProps) {
        const statusCode = req.path.startsWith('/error') ? parseInt(path.basename(req.path), 10) || 500 : 200;

        try {
          const html = renderToString(
            <Provider store={store} key="provider">
              <ReduxAsyncConnect {...renderProps} />
            </Provider>
          );
          const head = Helmet.rewind();
          const pageHtml = createPage(html, head, store.getState(), req.headers.host);
          const minifiedHtml = minify(pageHtml, { collapseWhitespace: true, minifyJS: true, minifyCSS: true });

          res.status(statusCode).send(minifiedHtml);
        } catch (err) {
          console.log(chalk.red(err.message));

          if (process.env.NODE_ENV === 'development') {
            res.status(500).send({ statusCode: 500, message: err.message, stack: err.stack.split('\n') });
          } else if (statusCode === 200) {
            res.redirect(`/error/500?url=${req.path}`);
          } else {
            // TODO: Send a pretty error html page
            res.status(500).send('Big time error son!');
          }
        }
      } else {
        res.redirect(`/error/404?url=${req.path}`);
      }
    });
  });
});

app.listen(app.get('port'), () => {
  console.log(
    '%s Express server listening on port %d in %s mode.',
    chalk.green('✓'), app.get('port'), app.get('env')
  );
});
