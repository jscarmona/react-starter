/* eslint import/no-extraneous-dependencies:0 */
/* eslint no-console: 0 */
/* eslint global-require: 0 */
const webpack = require('webpack');
const fs = require('fs-extra');
const path = require('path');
const config = require('../webpack.config');

const appDir = fs.realpathSync(process.cwd());
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

fs.emptyDirSync(path.resolve(appDir, 'build'));
fs.copySync(path.resolve(appDir, 'assets'), path.resolve(appDir, 'build', 'static'), {
  dereference: true,
});

const compiler = webpack(config);
compiler.run(() => {
  require('../build/server.js');
});
