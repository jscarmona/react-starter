/* eslint import/no-extraneous-dependencies:0 */
/* eslint no-console: 0 */
const webpack = require('webpack');
const chalk = require('chalk');
const fs = require('fs-extra');
const path = require('path');
const stripAnsi = require('strip-ansi');
const filesize = require('filesize');
const yargs = require('yargs');

const config = require('../webpack.config');

const appDir = fs.realpathSync(process.cwd());
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

function printFileSizes(stat) {
  const assets = stat.assets
    .filter((asset) => /\.(js|css)$/.test(asset.name))
    .map((asset) => ({
      name: path.basename(asset.name),
      sizeLabel: filesize(asset.size),
      size: asset.size,
    }));

  assets.sort((a, b) => b.size - a.size);

  const longestSizeLabelLength = Math.max.apply(null, assets.map(a => stripAnsi(a.sizeLabel).length));

  assets.forEach((asset) => {
    let sizeLabel = asset.sizeLabel;
    const sizeLength = stripAnsi(sizeLabel).length;

    if (sizeLength < longestSizeLabelLength) {
      const rightPadding = ' '.repeat(longestSizeLabelLength - sizeLength);
      sizeLabel += rightPadding;
    }

    console.log(`  ${sizeLabel}  ${chalk.cyan(asset.name)}`);
  });
}

function printErrors(summary, errors) {
  console.log(chalk.red(summary));
  console.log();
  errors.forEach((err) => {
    console.log(err.message || err);
    console.log();
  });
}

function handleWebpackCompile(err, stats) {
  if (err) {
    printErrors('Failed to compile.', [err]);
    process.exit(1);
  }

  stats.stats.forEach((stat) => {
    if (stat.compilation.errors.length) {
      printErrors('Failed to compile.', stat.compilation.errors);
      process.exit(1);
    }
  });

  console.log(chalk.green('Compiled successfully.'));
  console.log();
  stats.toJson().children.forEach((stat) => {
    console.log(chalk.yellow(`${stat.name}.config.js`));
    printFileSizes(stat);
    console.log();
  });
}

// Start the build process
console.log(chalk.cyan(`Creating an optimized build for ${process.env.NODE_ENV}`));

// Empty build directory
fs.emptyDirSync(path.resolve(appDir, 'build'));

// Merge with the build folder
fs.copySync(path.resolve(appDir, 'assets'), path.resolve(appDir, 'build', 'static'), {
  dereference: true,
});

// Start the webpack build
const compiler = webpack(config);

if (yargs.argv.watch) {
  compiler.watch(null, (err, stats) => {
    handleWebpackCompile(err, stats);
    console.log(chalk.cyan('Watching src files to build'));
  });
} else {
  compiler.run(handleWebpackCompile);
}
