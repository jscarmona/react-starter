/* eslint import/no-extraneous-dependencies:0 */
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');
const ManifestPlugin = require('webpack-manifest-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const WebpackBuildNotifierPlugin = require('webpack-build-notifier');
const path = require('path');
const dotenv = require('dotenv');

const pkg = require('../package.json');

dotenv.config();

module.exports = {
  name: 'client',
  devtool: 'cheap-module-source-map',
  entry: {
    main: `${path.resolve(__dirname, '..', 'src')}/client.js`,
  },
  output: {
    path: path.resolve(__dirname, '..', 'build', 'static'),
    filename: 'js/[name].js',
    publicPath: '/',
  },
  resolve: {
    modules: [
      path.resolve(__dirname, '..', 'src'),
      path.resolve(__dirname, '..', 'node_modules'),
    ],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        include: path.resolve(__dirname, '..', 'src'),
        exclude: /(node_modules)/,
        loaders: ['babel-loader?cacheDirectory'],
      },
      {
        test: /\.(css|sass|scss)$/,
        loader: ExtractTextPlugin.extract({
          use: [
            { loader: 'css-loader', options: { sourceMap: true } },
            { loader: 'postcss-loader', options: { sourceMap: true } },
            {
              loader: 'sass-loader',
              options: {
                sourceMap: true,
                includePaths: [
                  path.resolve(__dirname, '..', 'src'),
                  path.resolve(__dirname, '..', 'node_modules'),
                ],
              },
            },
          ],
        }),
      },
    ],
  },
  plugins: [
    new WebpackBuildNotifierPlugin({ title: pkg.name, onClick: () => {} }),
    new webpack.LoaderOptionsPlugin({
      options: {
        context: __dirname,
        postcss: [autoprefixer({ browsers: ['last 2 versions'] })],
      },
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'js/vendor.js',
      minChunks: (module) => module.context && module.context.indexOf('node_modules') !== -1,
    }),
    new webpack.DefinePlugin({
      'process.env': Object.keys(process.env)
        .filter((env) => (/^APP_/i).test(env))
        .reduce((env, key) => (
          Object.assign({}, env, { [key]: JSON.stringify(process.env[key]) })
        ), {
          NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        }),
    }),
    new ExtractTextPlugin('css/[name].css'),
    new ManifestPlugin(),
  ],
};
