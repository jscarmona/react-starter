const path = require('path');

module.exports = {
  SRC_PATH: path.join(__dirname, '../..', 'src'),
  BUILD_PATH: path.join(__dirname, '../..', 'build'),
};
