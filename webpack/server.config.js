/* eslint import/no-extraneous-dependencies:0 */
const webpack = require('webpack');
const dotenv = require('dotenv');
const nodeExternals = require('webpack-node-externals');

const { SRC_PATH, BUILD_PATH } = require('./utils/paths.js');

dotenv.config();

module.exports = {
  name: 'server',
  target: 'node',
  node: {
    __dirname: true,
  },
  externals: [nodeExternals({ whitelist: ['react', 'react-dom'] })],
  entry: `${SRC_PATH}/server.js`,
  output: {
    path: BUILD_PATH,
    filename: 'server.js',
  },
  resolve: {
    modules: [SRC_PATH, 'node_modules'],
  },
  module: {
    rules: [
      {
        test: /\.(css|scss|sass)$/,
        loader: 'ignore-loader',
      },
      {
        test: /\.(js|jsx)$/,
        include: SRC_PATH,
        loader: 'babel-loader',
      },
    ],
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': Object.keys(process.env)
        .filter((env) => (/^APP_/i).test(env))
        .reduce((env, key) => Object.assign({}, env, { [key]: JSON.stringify(process.env[key]) }), {
          NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
        }),
    }),
  ],
};
